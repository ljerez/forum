# save this as app.py
from datetime import date
from operator import __getitem__
import ClassesAndMethods
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy as db
from sqlalchemy import Column, Integer, String, ForeignKey,Date,DateTime,create_engine
from sqlalchemy.orm import declarative_base, registry
from sqlalchemy.dialects.mysql import TINYINT, CHAR
from sqlalchemy.types import VARCHAR
from sqlalchemy.orm import sessionmaker
import hashlib
from ClassesAndMethods import *
from flask_login import LoginManager

def create_app():
    app = Flask(__name__, template_folder="templates")
    app.config['SECRET_KEY'] = 'tennico'


    from views import views;
    from auth import auth;

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')


    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)
    @login_manager.user_loader
    def load_user(id):
        return session.query(Users).get(int(id))


    return app 
#https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3#step-5-displaying-all-posts
