const http = require('http');
const fs = require('fs')
//creazione del server
const server=http.createServer((req,res)=>{
    //comunichiamo che il contenuto sarà di tipo html
    res.writeHead(200,{'Content-type': 'text/html'})
    //utilizziamo la libreria file system per leggere il file index.html e passare le informazioni all'interno di res, in caso di errore verrà richiamato l'errore 404 e in caso di successo all'interno della risposta verrà passato il file html
    fs.readFile('index.html',(error,data)=>{
        if(error){
            res.writeHead(404)
            res.write('errore')
        }else{
            res.write(data)
        }
        res.end()
    })
}).listen(3000,(error)=>{   //assegno la porta 3000 e in caso di errore lo comunico all'utente altrimenti tramite il terminale lo avviso
    if(error){
        console.error
    }else{
        console.log('server attivo sulla porta 3000')
    }
});
