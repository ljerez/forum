
from email.policy import default
from flask import Flask, render_template
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy as db
from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey,Date,DateTime,create_engine,func
from sqlalchemy.orm import declarative_base, registry, relationship
from sqlalchemy.dialects.mysql import TINYINT, CHAR
from sqlalchemy.types import VARCHAR
from sqlalchemy.orm import sessionmaker
import hashlib
#engine = db.create_engine('mysql+pymysql://e5:r00kieCla55@sportello.cerebotani.it/e5_jerez_forum', echo=False)
engine = db.create_engine('mysql+pymysql://luca:luca@localhost/forum', echo=False)
Session = sessionmaker(bind=engine)
session=Session()
metadata = db.MetaData()
threads = db.Table('threads', metadata, autoload=True, autoload_with=engine)
users = db.Table('users', metadata, autoload=True, autoload_with=engine)
Base = declarative_base()
#Base.query = session.query_property()
#query = db.select([threads])

#ResultProxy = session.execute(query)

#ResultSet = ResultProxy.fetchall()


class Threads(Base):
    __tablename__ = 'threads'

    id = Column(Integer, primary_key=True)
    name = Column('name',VARCHAR(60),unique=True)
    user_id=Column('user_id',Integer,ForeignKey('users.id'))
    creationDate=Column('creationDate', DateTime, default=func.now())
    posts = relationship("Posts")

class Users(Base, UserMixin):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column('name',VARCHAR(25))
    surname = Column('surname',VARCHAR(25))
    username = Column('username',VARCHAR(20))
    mail = Column('mail',VARCHAR(50))
    password = Column('password',CHAR(32))
    role=Column('role',VARCHAR(10))
    threads = relationship("Threads")
    def __repr__(self):
        return "<Users(name='%s', fullname='%s', nickname='%s')>" % (
                            self.name, self.surname, self.username)

class Posts(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    content = Column('content',VARCHAR(240))
    user_who_posted_id = Column('user_who_posted_id', Integer)
    postDate = Column('postDate', DateTime, default=func.now())
    thread_id = Column('thread_id',Integer,ForeignKey('threads.id'))
    is_bound = Column('is_bound',TINYINT(1))
    def __repr__(self):
        return "<Posts(id='%s', content='%s', postDate='%s')>" % (
                            self.id, self.content, self.postDate)



class Bindings(Base):
    __tablename__ = 'bindings'

    id = Column(Integer, primary_key=True)
    original_post_id = Column(Integer,)
    post_reply_id = Column(Integer, unique=True)


metadata.create_all(engine)

def encodePass(passw):
    return hashlib.md5(passw.encode()).hexdigest()

def insertThread(threadName,creatorId,creationDate):
    query = db.insert(Threads).values(name=threadName, user_id=creatorId, creationDate=creationDate)
    session.execute(query)
    session.commit()


def insertUser(realName,realSurname,userName, realMail, notEncodedPassword,userRole):
    query = db.insert(Users).values(name=realName, surname=realSurname, username=userName, mail=realMail, password=encodePass(notEncodedPassword), role=userRole)
    session.execute(query)
    session.commit()

def insertPost(nContent,origId,dateCompl, idThread, bound):
    query = db.insert(Users).values(content=nContent, user_who_posted_id=origId, postDate=dateCompl, thread_id=idThread, is_bound=bound)
    session.execute(query)
    session.commit()


def insertBinding(origpId,origrId):
    query = db.insert(Users).values(original_post_id=origpId, post_reply_id=origrId)
    session.execute(query)
    session.commit()

def logging(usrn):
    query = db.select(Users).filter_by(username=usrn)
    if query :
        session.execute(query)
        return True
    else:
        return False

def checkpass(passwordnew, usernam):
    query = db.select(Users).filter( Users.username==usernam, Users.password==encodePass(passwordnew))
    if query :
        session.execute(query)
        
        return True
    else:
        return False

def getPostsOfThread(threadId):
    query = db.select(Posts).filter(Posts.thread_id ==threadId)
    session.execute(query)
    return query

def getTableContent(tableName):
    results = session.execute(db.select([tableName])).fetchall()
    #results = Users.query.filter_by(id=1)
 #   for r in results:
  #      print (r)
    return results

notEncodedPassword="ciao"
#print(Users.__repr__)
#insertUser("Donald","Trump","MAGA","mypass","user")
getTableContent(Posts)
#insertThread("Computer e informatica",1,datetime.now())