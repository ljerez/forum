from unicodedata import category
from flask import Blueprint,render_template, redirect, url_for, request,flash
from ClassesAndMethods import Users, insertUser,db,session,encodePass, getTableContent
from werkzeug.security import check_password_hash
from flask_login import login_user, login_required, logout_user, current_user


auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        usrn = request.form.get('username')
        password = request.form.get('password')
        user = session.query(Users).filter_by(username=usrn).first()
        if user:
            if user.password == encodePass(password):
                login_user(user, remember=True)
                flash('Loggato!', category='success')
                return redirect(url_for('views.home'))
            else:
                flash('Password sbagliata.', category='error')
        else:
            flash('Mail inesistente.', category='error')

    return render_template("login.html", user=current_user)

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@auth.route('/sign-up', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        name = request.form.get('name')
        surname = request.form.get('surname')
        username = request.form.get('username')
        password = request.form.get('password1')
        cpassword = request.form.get('password2')
        if password != cpassword:
            flash('Cè qualcosa che non va.', category='error')
        elif len(password)<4:
            flash('Password troppo corta', category='error')
        else:
            insertUser(name, surname, username, email, password, "user")
            login_user(user, remember=True)
            flash("papapapa pa papapapa", category='success')
            return redirect(url_for('views.home'))

    return render_template("signup.html",user=current_user)