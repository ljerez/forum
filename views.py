from flask import Blueprint, render_template
from flask_login import login_required, current_user
from ClassesAndMethods import getTableContent,threads,getPostsOfThread

views = Blueprint('views', __name__)

@views.route('/')
@login_required
def home():
    threadList=getTableContent(threads)
    for t in threadList:
        print(t)
    return render_template("home.html",user=current_user,prova="ciao",tList=threadList)

@views.route('/thread/<id>')
@login_required
def see_thread(id):
    return render_template("threads.html",user=current_user,posts=getPostsOfThread(id))
